package com.example.bateriamaria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.bateriamaria.ui.login.LoginActivity;

public class SplashAngelica extends AppCompatActivity {

    static int TIMEOUT_MILLS = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_angelica);

        getSupportActionBar().hide(); // ocultar barra de arriba del telefono
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i =new Intent(SplashAngelica.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        },TIMEOUT_MILLS);
    }
}
